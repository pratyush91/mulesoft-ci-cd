# MuleSoft CI CD 

MuleSoft Cloud Hub Deployment using GITLAB CI CD pipeline

Table of content:
- Introduction
- Pre-requisite
- Creating a simple project in Anypoint Studio
- Configuring repository and action in GITLAB
- Running the pipeline


## Introduction

Modern day DevOps practice involves Continuous Development , Continuous Testing , Continuous Integration, Continuous Deployment,  and Continuous Monitoring. It bridges a gap between the development and operations activities by using automation in building, testing and deploying the application.

CI is intended to be used in combination with automated unit tests written through the practices of test-driven development.

CD focusses on delivering the functionalities frequently with an automated deployment.

This article shows a simple deployment scenario for mulesoft anypoint platform application using GITLAB for CICD to have a basic understanding on how the flow works.

## Pre-requisite

- Anypoint Platform account
- Anypoint Studio
- GITLAB account

## Creating a simple project in Anypoint Studio

- Open Anypoint studio, select the workspace and then right click on the project explorer.
<p><img src="/uploads/6524a6f6c852fb68930aa60cb7f3ef2e/image.png" width="50%" height="50%"></p>

- Add a project name(mule-cloudhub-ci-cd) and then click finish.
<p><img src="/uploads/983a99ac47f633985048fcf2995f4059/CI-anypoint_studio-2.png" width="50%" height="50%"></p>

- Once project is created then drag and drop the listener from the mule palette to the canvas. Add path to the listener configuration.
<p><img src="/uploads/c1afbd31be2d61afc4f684141cfee592/image.png" width="50%" height="50%"></p>

- Add the http listener configuration, add the port as ${http.port} and base path (if req.) and click ok.
<p><img src="/uploads/cce6efa80a255789a61941ae6172b0fb/image.png" width="50%" height="50%"></p>

- Save the configuration.
<p><img src="/uploads/e06c51ea9333a8a1bce29c0cfe4450e1/image.png" width="50%" height="50%"></p>

- Add a logger from the mule palette by dropping it inside the mule-ci-cdflow and add a message.
<p><img src="/uploads/4058e379d7a004709225359b1197b002/image.png" width="50%" height="50%"></p>

- Below is the basic pom.xml setting which gets created upon creation of the project.
<p><img src="/uploads/1b9b14dc46514483930b40fbe4948bfb/image.png" width="50%" height="50%"></p>

- To enable the deployment to cloud hub with CICD. Add the below configuration. You can see there are few variables that are dynamically passed like ${anypoint.username} and ${anypoint.password}, value for these fields we will define in the variable section of the GITLAB(explained below).
<p><img src="/uploads/900631ad7450b423080810fd38aa1940/image.png" width="50%" height="50%"></p>

Update below plugin
```
<plugin>
    <groupId>org.mule.tools.maven</groupId>
    <artifactId>mule-maven-plugin</artifactId>
    <version>${mule.maven.plugin.version}</version>
    <extensions>true</extensions>
</plugin>
```
With an additional configuration tag to enable cloud hub deployment.

```
<plugin>
    <groupId>org.mule.tools.maven</groupId>
    <artifactId>mule-maven-plugin</artifactId>
    <version>${mule.maven.plugin.version}</version>
    <extensions>true</extensions>
    <configuration>
        <classifier>mule-application</classifier>
        <cloudHubDeployment>
            <uri>https://anypoint.mulesoft.com</uri>
            <muleVersion>4.3.0</muleVersion>
            <username>${anypoint.username}</username>
            <password>${anypoint.password}</password>
            <applicationName>${appName}</applicationName>
            <environment>Sandbox</environment>
            <workerType>Micro</workerType>
            <workers>1</workers>
            <objectStoreV2>true</objectStoreV2>
        </cloudHubDeployment>
    </configuration>
</plugin>
```

## Configuring repository and action in GITLAB

- Login to GITLAB and create a blank project with name of your choice.
<p><img src="/uploads/91c60c6ab6c5851ed887328291799f77/image.png" width="50%" height="50%"></p>

- Click on clone and copy either with git ssh or https url. I have selected https right for the demo.
<p><img src="/uploads/c08cf67338eaedce4a482bd71e1a6a3f/image.png" width="50%" height="50%"></p>

- On your local, go to terminal and go to the anypoint studio project directory.
- Do git init at the root folder of the project to initialize the repository.`git init`
- Add git remote origin to the folder.`git remote add origin <git repo url>`
- Git status to validate if the folders are under git repo and which branch are you currently on. `git status`
- If you see a mismatch in the branch, like master on your local and main branch in gitlab repository. Then do `git pull origin main` .

Now we can update the git repository with our anypoint code. So to do that, we need to follow the below git commands:

```
git add . // add all files to stage
git commit -m "Commit message"
git push origin master
```
Now there will be a new branch(master) created in the gitlab.

## Running the pipeline

To add the runtime variables which were defined in the property tag for cloud hub go to -> project/settings/ci-cd. 

<p>Expand the variable section and add the key and values to it. To access anypoint platform it's USERNAME and PASSWORD have been added here and referenced in the script as $USERNAME and $PASSWORD. </p>

<p><img src="/uploads/6c852303a01343c341556496a2655f2d/image.png" width="50%" height="50%"></p>

<p>Make sure that if you make the values as protected then the branch should also be protected else you won’t be able to fetch the values in the pipeline. For understanding purpose I haven’t marked the value as protected.</p>

<p> Now to create the DevOps pipeline, go to pipelines under CI/CD. Select on use template. Here as we have changed the branch from main to master you can select master from the drop down and select on create new pipeline.</p>

<p><img src="/uploads/e39529010a3a939b28cad3e59853825b/image.png" width="50%" height="50%"></p>

For a basic deployment to cloud hub you can use the below command in the editor.

<p><img src="/uploads/12f698190fc8cd95298061b3ddca9936/image.png" width="50%" height="50%"></p>

```
image: maven:3.8.2-jdk-8

stages:
    - deploy

deploy:
    stage: deploy
    script:
        - mvn package deploy -DmuleDeploy -Denvironment=Sandbox -DappName=MuleCICD -Danypoint.username=$USERNAME -Danypoint.password=$PASSWORD -DmuleVersion=4.3.0 -Dworkers=1 -DvCore=Micro
```
You can enhance the script by adding multiple stages and variable for your project use as well.

Once you commit the changes post this the pipeline will trigger which you can refer under pipeline -> Jobs

Post commit you will be seeing a pipeline trigger would have started under the branch drop down tab
<p><img src="/uploads/1a24a9daf63e26a7e6dc1bf8fc11b5ba/image.png" width="50%" height="50%"></p>

You can view the pipeline logs under Pipeline -> Jobs

Once the build succeeds, you can login to the Anypoint platform and validate the deployment.
<p><img src="/uploads/40a3eb75eb1e28fe20ea4be614c67510/image.png" width="50%" height="50%"></p>

You can use curl or postman to call the endpoint, replace the app url before making the below call.

`curl --location --request GET 'mulecicd.us-e2.cloudhub.io/cloudhub/cicd'`

You can verify the call from anypoint platform as well. Click on the name of the application and then click on logs to find the logger value that was set.
<p><img src="/uploads/a6b15b4b18baf097348f44a7b7bcd80e/image.png" width="50%" height="50%"></p>

GITLAB free version have a certain free limit for running the pipeline, post which you will have to buy pipeline minutes to run your builds.


